# php-net
Complete PHP Networking Toolkit

First make a folder and put downloaded files there. Then Edit your composer file.

```
"autoload": {
        "psr-4": {
            ...,
            "MDMasudSikdar71\\PhpNetWorking\\": "YOUR_PROJECT_FOLDER_PATH/src/",
            ...,
        }
    },
```

After that 

``composer dump-autoload``

## Socket Server Side.
```php
use MDMasudSikdar71\PhpNetWorking\network\Socket;
try {
    $server = new Socket(SERVER_IP, SERVER_PORT, [
        'bind' => true,
        'listen' => true
    ]);
    echo "Server initiated... \n";
    $server->startServer('', function ($message) {
        $response = [$message]; // return your custom message;
        $response = json_encode($response);
        return $response;
    }, 'closure');

} catch (\Exception $exception) {
    echo $exception->getMessage() . "\n";
}
```
## Socket Client Side declaration.
```php
use MDMasudSikdar71\PhpNetWorking\network\Socket;
$request = 1;
$start = microtime(true);
for ($i = 0; $i < $request; $i++) {
    $socket = new Socket(SERVER_IP, SERVER_PORT, ['connect' => true]);
    $response = $socket->send(json_encode(["hello"]));
    echo $response;
    $socket->close();
}
echo "\n" . 'Execution Time: ' . (microtime(true) - $start) . "\n";
```
